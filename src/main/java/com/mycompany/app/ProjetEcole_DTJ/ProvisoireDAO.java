package com.mycompany.app.ProjetEcole_DTJ;

import java.sql.*;

public class ProvisoireDAO {
	
	private Connection connexion;
	private final String adresse = "jdbc:sqlite:livres.sqlite";
	
	/**
	 * Data Access Object est l'intermédiaire d'ouverture des connexions avec la basse 
	 * @throws SQLException
	 */
	public ProvisoireDAO() throws SQLException {
		Connection c = DriverManager.getConnection(adresse);
		this.connexion = c;
	}
	
	/**
	 * La méthode insereLivre adapte les paramètres aux données saisies dans l'interface
	 * @param titre
	 * @param annee
	 * @param editeur
	 * @param prenomAuteur
	 * @param nomAuteur
	 * @throws SQLException
	 */
	public void insereLivre(String titre, int annee, String editeur, String prenomAuteur, String nomAuteur) throws SQLException {
		Statement st = connexion.createStatement();
		st.executeUpdate("CREATE TABLE IF NOT EXISTS livres(idLivre INTEGER PRIMARY KEY, titre TEXT, annee INTEGER, editeur TEXT, prenomAuteur TEXT, nomAuteur TEXT)");
		PreparedStatement ps = connexion.prepareStatement("INSERT INTO livres(titre, annee, editeur, prenomAuteur, nomAuteur) VALUES (?,?,?,?,?)");
		ps.setString(1, titre);
		ps.setInt(2, annee);
		ps.setString(3, editeur);
		ps.setString(4, prenomAuteur);
		ps.setString(5, nomAuteur);
		ps.executeUpdate();
	}
	
	public void afficheTable() throws SQLException {
		PreparedStatement ps2 = connexion.prepareStatement("SELECT * FROM livres");
		ResultSet rs = ps2.executeQuery();
		for(int i=0; i< rs.getMetaData().getColumnCount(); i++){ 
			//Affiche noms des colonnes
		    String nomColonne = rs.getMetaData().getColumnName(i+1); 
		    System.out.print(String.format("%s | ", nomColonne));
		}
		System.out.println();
		while (rs.next()) {
			// Tant qu'on peut passer à la donnée suivante...
			System.out.println(String.format("%d | %s | %d", rs.getInt("idLivre"), rs.getString("titre"), rs.getInt("annee")));
		}
		rs.close();
	}
	
	public void ferme() throws SQLException {
		connexion.close();
	}	

}
