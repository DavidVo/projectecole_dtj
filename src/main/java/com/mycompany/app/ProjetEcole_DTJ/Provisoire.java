package com.mycompany.app.ProjetEcole_DTJ;

import com.mycompany.app.ProjetEcole_DTJ.ProvisoireDAO;

public class Provisoire {
	
	static void faireLivre() {
		java.util.Scanner sc = new java.util.Scanner(System.in);
		try {
			ProvisoireDAO dao = new ProvisoireDAO();
			
			System.out.println("titre :");
			String titre = sc.next();
			System.out.println("annee :");
			int annee = sc.nextInt();
			
			String editeur = "rrr";
			String nomAuteur = "M";
			String prenomAuteur = "H";
			
			sc.close();
		
			dao.insereLivre(titre, annee, editeur, prenomAuteur, nomAuteur);	
			
			dao.afficheTable();
			dao.ferme();
			
		} catch (StringIndexOutOfBoundsException ex) {
			System.out.println("Erreur : " + ex);
			faireLivre();
		} catch (Exception ex) {
			System.out.println("Erreur : " + ex);
		}
	}

}
