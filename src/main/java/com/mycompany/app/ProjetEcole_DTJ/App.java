package com.mycompany.app.ProjetEcole_DTJ;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Bienvenue à notre bibliothèque" );
        
        Bibliotheque_Frames.faire_Livre();
        
        Bibliotheque_Frames.faire_Usager();
        
        Bibliotheque_Frames.faire_Emprunt();
        
    }
}
