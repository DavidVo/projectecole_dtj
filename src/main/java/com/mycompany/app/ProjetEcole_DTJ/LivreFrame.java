package com.mycompany.app.ProjetEcole_DTJ;
<<<<<<< HEAD

import java.awt.Color;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class LivreFrame extends JFrame {
	
	Font font_titre = new Font("SansSerif", Font.BOLD, 40);
	Font font_Label = new Font("SansSerif", Font.PLAIN, 40);
	
	public LivreFrame() {
		
		int SizeX = 500;
		int SizeY = 700;
		int BaseY = 100;
		int EspaceY = 80;
		
		// Création de la fenetre d'ajout de livre
		JFrame frame = new JFrame("Livre");
		
		setSize(SizeX,SizeY);
		setLocation(50,200);
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // Permet d'arreter l'application quand on ferme la fenetre
		getContentPane().setLayout(null); // indique pas de redimmossionnement de la fenetre
		getContentPane().setBackground(Color.darkGray);
		
		// Création des différents lable de cette fenetre
		JLabel label_AajLivre = new JLabel("Ajout Livre");
		getContentPane().add(label_AajLivre);
		label_AajLivre.setFont(font_titre);
		label_AajLivre.setForeground(Color.white);
		label_AajLivre.setBackground(Color.darkGray);
		label_AajLivre.setBounds((SizeX/2)-(250/2),0,250,100);
		
		JLabel label_titre = new JLabel("Titre");
		getContentPane().add(label_titre);
		label_titre.setFont(font_Label);
		label_titre.setForeground(Color.LIGHT_GRAY);
		label_titre.setBackground(Color.darkGray);
		label_titre.setBounds(20,BaseY,250,100);
		
		JLabel label_annee = new JLabel("Année");
		getContentPane().add(label_annee);
		label_annee.setFont(font_Label);
		label_annee.setForeground(Color.LIGHT_GRAY);
		label_annee.setBackground(Color.darkGray);
		label_annee.setBounds(20,(BaseY+(1*EspaceY)),250,100);
		
		JLabel label_ed = new JLabel("Editeur");
		getContentPane().add(label_ed);
		label_ed.setFont(font_Label);
		label_ed.setForeground(Color.LIGHT_GRAY);
		label_ed.setBackground(Color.darkGray);
		label_ed.setBounds(20,(BaseY+(2*EspaceY)),250,100);
		
		JLabel label_nom = new JLabel("Nom");
		getContentPane().add(label_nom);
		label_nom.setFont(font_Label);
		label_nom.setForeground(Color.LIGHT_GRAY);
		label_nom.setBackground(Color.darkGray);
		label_nom.setBounds(20,(BaseY+(3*EspaceY)),250,100);
		
		JLabel label_prenom = new JLabel("Prenom");
		getContentPane().add(label_prenom);
		label_prenom.setFont(font_Label);
		label_prenom.setForeground(Color.LIGHT_GRAY);
		label_prenom.setBackground(Color.darkGray);
		label_prenom.setBounds(20,(BaseY+(4*EspaceY)),250,100);
		
		
		// Création des textfield de cette fenetre
		JTextField TextField_titre = new JTextField();
		getContentPane().add(TextField_titre); 
		TextField_titre.setBackground(Color.white);
		TextField_titre.setBounds(200,(BaseY+30),250,40);
		
		JTextField TextField_annee = new JTextField();
		getContentPane().add(TextField_annee); 
		TextField_annee.setBackground(Color.white);
		TextField_annee.setBounds(200,(BaseY+30)+(1*EspaceY),250,40);
		
		JTextField TextField_ed = new JTextField();
		getContentPane().add(TextField_ed); 
		TextField_ed.setBackground(Color.white);
		TextField_ed.setBounds(200,(BaseY+30)+(2*EspaceY),250,40);
		
		JTextField TextField_nom = new JTextField();
		getContentPane().add(TextField_nom); 
		TextField_nom.setBackground(Color.white);
		TextField_nom.setBounds(200,(BaseY+30)+(3*EspaceY),250,40);
		
		JTextField TextField_prenom = new JTextField();
		getContentPane().add(TextField_prenom); 
		TextField_prenom.setBackground(Color.white);
		TextField_prenom.setBounds(200,(BaseY+30)+(4*EspaceY),250,40);
		
		// Creation du bouton d'enregistrement
		JButton Button_save = new JButton("Enregistrer");
		getContentPane().add(Button_save); // Ajoute le bouton 
		Button_save.setFont(font_Label);
		Button_save.setBackground(Color.LIGHT_GRAY);
		Button_save.setBounds((SizeX/2)-(300/2),550,300,60);
		
		// Creation de la fenetre
		setVisible(true);

	}

}
=======
import com.mycompany.app.ProjetEcole_DTJ.ProvisoireDAO;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;


public class LivreFrame extends JFrame implements ActionListener {
	
	// Création de la fenetre d'ajout de livre
	JFrame frame = new JFrame("Livre");
	
	// Propriétés graphiques
	Font font_titre = new Font("SansSerif", Font.BOLD, 40);
	Font font_Label = new Font("SansSerif", Font.PLAIN, 40);
	
	// Composants graphiques
	JTextField TextField_titre = new JTextField();
	JTextField TextField_annee = new JTextField();
	JTextField TextField_ed = new JTextField();
	JTextField TextField_nom = new JTextField();
	JTextField TextField_prenom = new JTextField();
	JButton Button_save = new JButton("Enregistrer");
	
	public LivreFrame() {
		
		int SizeX = 500;
		int SizeY = 700;
		int BaseY = 100;
		int EspaceY = 80;
		
		setSize(SizeX,SizeY);
		setLocation(50,200);
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // Permet d'arrêter l'application quand on ferme la fenêtre
		getContentPane().setLayout(null); // indique pas de redimensionnement de la fenetre
		getContentPane().setBackground(Color.darkGray);
		
		// =======================================================================================================================
		// Création des différents labels de cette fenêtre
		JLabel label_AajLivre = new JLabel("Ajout Livre");
		getContentPane().add(label_AajLivre);
		label_AajLivre.setFont(font_titre);
		label_AajLivre.setForeground(Color.white);
		label_AajLivre.setBackground(Color.darkGray);
		label_AajLivre.setBounds((SizeX/2)-(250/2),0,250,100);
		
		JLabel label_titre = new JLabel("Titre");
		getContentPane().add(label_titre);
		label_titre.setFont(font_Label);
		label_titre.setForeground(Color.LIGHT_GRAY);
		label_titre.setBackground(Color.darkGray);
		label_titre.setBounds(20,BaseY,250,100);
		
		JLabel label_annee = new JLabel("Année");
		getContentPane().add(label_annee);
		label_annee.setFont(font_Label);
		label_annee.setForeground(Color.LIGHT_GRAY);
		label_annee.setBackground(Color.darkGray);
		label_annee.setBounds(20,(BaseY+(1*EspaceY)),250,100);
		
		JLabel label_ed = new JLabel("Editeur");
		getContentPane().add(label_ed);
		label_ed.setFont(font_Label);
		label_ed.setForeground(Color.LIGHT_GRAY);
		label_ed.setBackground(Color.darkGray);
		label_ed.setBounds(20,(BaseY+(2*EspaceY)),250,100);
		
		JLabel label_nom = new JLabel("Nom");
		getContentPane().add(label_nom);
		label_nom.setFont(font_Label);
		label_nom.setForeground(Color.LIGHT_GRAY);
		label_nom.setBackground(Color.darkGray);
		label_nom.setBounds(20,(BaseY+(3*EspaceY)),250,100);
		
		JLabel label_prenom = new JLabel("Prenom");
		getContentPane().add(label_prenom);
		label_prenom.setFont(font_Label);
		label_prenom.setForeground(Color.LIGHT_GRAY);
		label_prenom.setBackground(Color.darkGray);
		label_prenom.setBounds(20,(BaseY+(4*EspaceY)),250,100);
		
		// =======================================================================================================================
		// Création des textfield de cette fenetre

		getContentPane().add(TextField_titre); 
		TextField_titre.setBackground(Color.white);
		TextField_titre.setBounds(200,(BaseY+30),250,40);
		
		getContentPane().add(TextField_annee); 
		TextField_annee.setBackground(Color.white);
		TextField_annee.setBounds(200,(BaseY+30)+(1*EspaceY),250,40);
		
		getContentPane().add(TextField_ed); 
		TextField_ed.setBackground(Color.white);
		TextField_ed.setBounds(200,(BaseY+30)+(2*EspaceY),250,40);
		
		getContentPane().add(TextField_nom); 
		TextField_nom.setBackground(Color.white);
		TextField_nom.setBounds(200,(BaseY+30)+(3*EspaceY),250,40);
		
		getContentPane().add(TextField_prenom); 
		TextField_prenom.setBackground(Color.white);
		TextField_prenom.setBounds(200,(BaseY+30)+(4*EspaceY),250,40);
		
		// =======================================================================================================================
		// Creation du bouton d'enregistrement
		getContentPane().add(Button_save); // Ajoute le bouton 
		Button_save.setFont(font_Label);
		Button_save.setBackground(Color.LIGHT_GRAY);
		Button_save.setBounds((SizeX/2)-(300/2),550,300,60);
		Button_save.addActionListener(this);
		
		// =======================================================================================================================
		// Creation de la fenetre
		setVisible(true);

	}
	
	/**
	 * Méthode associée à l'évènement "cliquer sur le bouton Button_save"
	 */
	public void actionPerformed(ActionEvent arg0) {
		
		try {
			ProvisoireDAO dao = new ProvisoireDAO();
			
			String titre = TextField_titre.getText();
			int annee = Integer.valueOf(TextField_annee.getText());
			String editeur = TextField_ed.getText();
			String nomAuteur = TextField_nom.getText();
			String prenomAuteur = TextField_prenom.getText();
		
			dao.insereLivre(titre, annee, editeur, prenomAuteur, nomAuteur);	
			
			dao.afficheTable();
			dao.ferme();
			
		} catch (StringIndexOutOfBoundsException ex) {
			System.out.println("Erreur : " + ex);
			BibliothequeFrames.faireLivre();
		} catch (Exception ex) {
			System.out.println("Erreur : " + ex);
		}
	}

}

>>>>>>> refs/remotes/origin/Julien
