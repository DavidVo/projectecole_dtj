package com.mycompany.app.ProjetEcole_DTJ;

<<<<<<< HEAD
import javax.swing.JFrame;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;

public class EmpruntFrame extends JFrame {

	Font font_titre = new Font("SansSerif", Font.BOLD, 40);
	Font font_Label = new Font("SansSerif", Font.PLAIN, 40);

	public EmpruntFrame() {

		int SizeX = 500;
		int SizeY = 700;
		int BaseY = 100;
		int EspaceY = 80;

		// Création de la fenetre d'ajout de livre
		JFrame frame = new JFrame("Livre");

		setSize(SizeX, SizeY);
		setLocation(50, 200);

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // Permet d'arreter l'application quand on ferme la fenetre
		getContentPane().setLayout(null); // indique pas de redimmossionnement de la fenetre
		getContentPane().setBackground(Color.darkGray);

		// Création des différents lable de cette fenetre
		JLabel labeltitre = new JLabel("Faire un emprunt");
		getContentPane().add(labeltitre);
		labeltitre.setFont(font_titre);
		labeltitre.setForeground(Color.white);
		labeltitre.setBackground(Color.darkGray);
		labeltitre.setBounds((SizeX / 2) - (300 / 2), 0, 400, 100);

		JLabel labellivre = new JLabel("Livre");
		getContentPane().add(labellivre);
		labellivre.setFont(font_Label);
		labellivre.setForeground(Color.LIGHT_GRAY);
		labellivre.setBackground(Color.darkGray);
		labellivre.setBounds(20, BaseY, 250, 100);

		JLabel labelid = new JLabel("ID usager");
		getContentPane().add(labelid);
		labelid.setFont(font_Label);
		labelid.setForeground(Color.LIGHT_GRAY);
		labelid.setBackground(Color.darkGray);
		labelid.setBounds(20, (BaseY + (1 * EspaceY)), 250, 100);

		JLabel labeldate = new JLabel("Date");
		getContentPane().add(labeldate);
		labeldate.setFont(font_Label);
		labeldate.setForeground(Color.LIGHT_GRAY);
		labeldate.setBackground(Color.darkGray);
		labeldate.setBounds(20, (BaseY + (2 * EspaceY)), 250, 100);

		
		// Création des textfield de cette fenetre
		JTextField textFieldlivre = new JTextField();
		getContentPane().add(textFieldlivre);
		textFieldlivre.setBackground(Color.white);
		textFieldlivre.setBounds(200, (BaseY + 30), 250, 40);

		JTextField textFieldid = new JTextField();
		getContentPane().add(textFieldid);
		textFieldid.setBackground(Color.white);
		textFieldid.setBounds(200, (BaseY + 30) + (1 * EspaceY), 250, 40);

		JTextField textFielddate = new JTextField();
		getContentPane().add(textFielddate);
		textFielddate.setBackground(Color.white);
		textFielddate.setBounds(200, (BaseY + 30) + (2 * EspaceY), 250, 40);

		//DefaultListModel listemprunt = new DefaultListModel(); //data has type Object[]
		//getContentPane().add(int index, E element);
		
		//JScrollPane listScroller = new JScrollPane(listemprunt);
		//listScroller.setPreferredSize(new Dimension(250, 80));
=======
import javax.swing.*;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

public class EmpruntFrame extends JFrame {

	Font font_titre = new Font("SansSerif", Font.BOLD, 40);
	Font font_Label = new Font("SansSerif", Font.PLAIN, 40);

	public EmpruntFrame() {

		int SizeX = 500;
		int SizeY = 700;
		int BaseY = 100;
		int EspaceY = 80;

		// Création de la fenetre d'ajout de livre
		JFrame frame = new JFrame("Livre");

		setSize(SizeX, SizeY);
		setLocation(50, 200);

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // Permet d'arreter l'application quand on ferme la fenetre
		getContentPane().setLayout(null); // indique pas de redimmossionnement de la fenetre
		getContentPane().setBackground(Color.darkGray);

		// Création des différents lable de cette fenetre
		JLabel labeltitre = new JLabel("Faire un emprunt");
		getContentPane().add(labeltitre);
		labeltitre.setFont(font_titre);
		labeltitre.setForeground(Color.white);
		labeltitre.setBackground(Color.darkGray);
		labeltitre.setBounds((SizeX / 2) - (300 / 2), 0, 400, 100);

		JLabel labellivre = new JLabel("Livre");
		getContentPane().add(labellivre);
		labellivre.setFont(font_Label);
		labellivre.setForeground(Color.LIGHT_GRAY);
		labellivre.setBackground(Color.darkGray);
		labellivre.setBounds(20, BaseY, 250, 100);

		JLabel labelid = new JLabel("ID usager");
		getContentPane().add(labelid);
		labelid.setFont(font_Label);
		labelid.setForeground(Color.LIGHT_GRAY);
		labelid.setBackground(Color.darkGray);
		labelid.setBounds(20, (BaseY + (1 * EspaceY)), 250, 100);

		JLabel labeldate = new JLabel("Date");
		getContentPane().add(labeldate);
		labeldate.setFont(font_Label);
		labeldate.setForeground(Color.LIGHT_GRAY);
		labeldate.setBackground(Color.darkGray);
		labeldate.setBounds(20, (BaseY + (2 * EspaceY)), 250, 100);

		
		// Création des textfield de cette fenetre
		JTextField textFieldlivre = new JTextField();
		getContentPane().add(textFieldlivre);
		textFieldlivre.setBackground(Color.white);
		textFieldlivre.setBounds(200, (BaseY + 30), 250, 40);

		JTextField textFieldid = new JTextField();
		getContentPane().add(textFieldid);
		textFieldid.setBackground(Color.white);
		textFieldid.setBounds(200, (BaseY + 30) + (1 * EspaceY), 250, 40);

		JTextField textFielddate = new JTextField();
		getContentPane().add(textFielddate);
		textFielddate.setBackground(Color.white);
		textFielddate.setBounds(200, (BaseY + 30) + (2 * EspaceY), 250, 40);

		// DefaultListModel listemprunt = new DefaultListModel(); //data has type Object[]
		// getContentPane().add(int index, E element);
		
		JScrollPane listScroller = new JScrollPane(listemprunt);
		listScroller.setPreferredSize(new Dimension(250, 80));
>>>>>>> refs/remotes/origin/Julien
		

		// Creation du bouton d'enregistrement
		JButton Button_save = new JButton("Enregistrer");
		getContentPane().add(Button_save); // Ajoute le bouton
		Button_save.setFont(font_Label);
		Button_save.setBackground(Color.LIGHT_GRAY);
		Button_save.setBounds((SizeX / 2) - (300 / 2), 550, 300, 60);

		// Creation de la fenetre
		setVisible(true);
	}
}
