package com.mycompany.app.ProjetEcole_DTJ;

import java.awt.Color;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;


public class Usager_Frame extends JFrame {
	Font font_titre = new Font("SansSerif", Font.BOLD, 40);
	Font font_Label = new Font("SansSerif", Font.PLAIN, 40);
	
	public Usager_Frame() {
		
		int SizeX = 520;
		int SizeY = 600;
		int BaseY = 100;
		int EspaceY = 80;
		
		// Création de la fenetre d'ajout de livre
		JFrame frame = new JFrame("Livre");
		
		setSize(SizeX,SizeY);
		setLocation(1000,200);
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE); // Permet d'arreter l'application quand on ferme la fenetre
		getContentPane().setLayout(null); // indique pas de redimmossionnement de la fenetre
		getContentPane().setBackground(Color.darkGray);
		
		// Création des différents lable de cette fenetre
		JLabel label_AajLivre = new JLabel("Nouveau usager");
		getContentPane().add(label_AajLivre);
		label_AajLivre.setFont(font_titre);
		label_AajLivre.setForeground(Color.white);
		label_AajLivre.setBackground(Color.darkGray);
		label_AajLivre.setBounds((SizeX/2)-(310/2),0,310,100);
		
		JLabel label_nom = new JLabel("Nom");
		getContentPane().add(label_nom);
		label_nom.setFont(font_Label);
		label_nom.setForeground(Color.LIGHT_GRAY);
		label_nom.setBackground(Color.darkGray);
		label_nom.setBounds(20,BaseY,250,100);
		
		JLabel label_prenom = new JLabel("Prenom");
		getContentPane().add(label_prenom);
		label_prenom.setFont(font_Label);
		label_prenom.setForeground(Color.LIGHT_GRAY);
		label_prenom.setBackground(Color.darkGray);
		label_prenom.setBounds(20,(BaseY+(1*EspaceY)),250,100);
		
		JLabel label_tel = new JLabel("Téléphone");
		getContentPane().add(label_tel);
		label_tel.setFont(font_Label);
		label_tel.setForeground(Color.LIGHT_GRAY);
		label_tel.setBackground(Color.darkGray);
		label_tel.setBounds(20,(BaseY+(2*EspaceY)),250,100);
		
		JLabel label_email = new JLabel("Email");
		getContentPane().add(label_email);
		label_email.setFont(font_Label);
		label_email.setForeground(Color.LIGHT_GRAY);
		label_email.setBackground(Color.darkGray);
		label_email.setBounds(20,(BaseY+(3*EspaceY)),250,100);

		
		
		// Création des textfield de cette fenetre
		JTextField TextField_nom = new JTextField();
		getContentPane().add(TextField_nom); 
		TextField_nom.setBackground(Color.white);
		TextField_nom.setBounds(230,(BaseY+30),250,40);
		
		JTextField TextField_prenom = new JTextField();
		getContentPane().add(TextField_prenom); 
		TextField_prenom.setBackground(Color.white);
		TextField_prenom.setBounds(230,(BaseY+30)+(1*EspaceY),250,40);
		
		JTextField TextField_tel = new JTextField();
		getContentPane().add(TextField_tel); 
		TextField_tel.setBackground(Color.white);
		TextField_tel.setBounds(230,(BaseY+30)+(2*EspaceY),250,40);
		
		JTextField TextField_email = new JTextField();
		getContentPane().add(TextField_email); 
		TextField_email.setBackground(Color.white);
		TextField_email.setBounds(230,(BaseY+30)+(3*EspaceY),250,40);
		
		
		// Creation du bouton d'enregistrement
		JButton Button_save = new JButton("Enregistrer");
		getContentPane().add(Button_save); // Ajoute le bouton 
		Button_save.setFont(font_Label);
		Button_save.setBackground(Color.LIGHT_GRAY);
		Button_save.setBounds((SizeX/2)-(300/2),460,300,60);
		
		// Creation de la fenetre
		setVisible(true);

	}

}